#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import math

class Point:
    """ blabla """
    def __init__(self, x, y):
        self.__x = x
        self.__y = y
        
    def x(self):
        return self.__x
    
    def y(self):
        return self.__y
    
    def r(self):
        return math.sqrt(self.x()**2 + self.y()**2) 
    
    def t(self):
        return math.atan2(self.y(), self.x())
    
    def __str__(self):
        return '(' + str(self.x()) + ',' + str(self.y()) + ')'
    
    def __eq__(self, b):
        return isinstance(b, Point) and self.x() == b.x() and self.y() == b.y()
    
    def homothetie(self, k):
        self.__x = self.x() * k
        self.__y = self.y() * k
    
    def translation(self, dx, dy):
        self.__x = self.x() + dx
        self.__y = self.y() + dy
        
    def rotation(self, a):
        t = self.t() + a
        self.__x = self.r() * math.cos(t)
        self.__y = self.r() * math.sin(t)


class Polygone:
    def __init__(self, sommets):
        self.sommets = sommets
        
    def sommets(self):
        return self.sommets
    
    def __str__(self):
        liste = ''
        for i in self.sommets:
            liste = liste + str(i)
        return 'Coordonnées de la ligne polygonale: ' + liste
        
    def get_sommets(self, i):
        return str(self.sommets[i])
    
    def aire(self):
        area = 0
        for i in range(len(self.sommets)-1):
            a = self.sommets[i].x() + self.sommets[i+1].x()
            b = self.sommets[i].y() - self.sommets[i+1].y()
            mult = a*b
            area += mult/2


class Triangle(Polygone):
    def __init__(self, a, b, c):
        self.sommets = [a, b, c]


class Rectangle(Polygone):
    def __init__(self, xMin, xMax, yMin, yMax):
        #Polygone.__init__(self, xMin, xMax, yMin, yMax)
        a = Point(xMin, yMin)
        b = Point(xMax, yMax)
        c = Point(xMin, yMax)
        d = Point(xMax, yMin)
        self.sommets = [a, b, c, d]


a = Triangle(Point(0,1), Point(3,4), Point(6,10))
print(a)